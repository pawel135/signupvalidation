var author = "Paweł Szczepanowski";

$(function(){
	initialize();
	addListeners();
	validate();
});

function initialize(){
        $('#validatorW3').attr('href', 'https://validator.w3.org/check?uri=' + window.location.href +'&output=json');      
        $('#signup input:radio[name="sex"][value="female"]').prop('checked', true);
	$('form#signup input:first').focus();
}

function addListeners(){
	$('header').click( function(){
		$('#signup').slideToggle(1000);
		$('#form_header').fadeToggle(900);
	});

	$('#pesel').focusout(function(){
		if ( $('#pesel').val().length > 0 && validator.check('#pesel') ){
			fillBirtdateAndGenderBasedOnPESEL();
		}
	});
	addAnimationListeners();
}

function fillBirtdateAndGenderBasedOnPESEL(){
	var pesel = $('#pesel').val();
	var day = pesel.substring(4,6);
	var month = pesel.substring(2,4);
	var year = pesel.substring(0,2) ;
	var currentYear = new Date().getFullYear();
	if ( month <= 12 ){
		year = "19"+year;
	} else if ( month <= 32 ){
		year = "20"+year;
	} else if ( month <= 52 ){
		year = "21"+year;
	} else if ( month <= 72 ){
		year = "22"+year;
	} else if ( month <= 92 ){
		year = "18"+year;
	}
	month = parseInt(month)%20;

	$('#birthdate').val(month+"/"+day+"/"+year);
	var genderFromPESEL =  ( parseInt( pesel[pesel.length-2] ) % 2 == 0) ? "female" : "male";

	$('#signup input:radio[name="sex"][value="'+ genderFromPESEL +'"]').prop('checked', true);
}

function addAnimationListeners(){
	$('#signup .label').click( function(){
		$(this).hide().animate({
			opacity: 0.95,
			left: "+=10",
			height: [ "toggle", "swing" ]
			//				height: "toggle"
		}, {
			duration: 500,
			specialEasing: {
				width: "linear",
				height: "easeOutBounce"
			}
		}, function() {

		});
	});
}


function validate(){
	addCustomMethods();
	validator = $('#signup').validate({
		ignore: ":hidden",
//		onkeyup: false,
                onkeyup: function(element) {
                  var element_id = $(element).attr('id');
                  if (this.settings.rules[element_id].onkeyup !== false) {
                    $.validator.defaults.onkeyup.apply(this, arguments);
                  }
                },
		onfocusout: function(element) {
			this.element(element);
		},
	/*	onchange: function(element){
			this.element(element);
		},
	*/	rules:{
			login: {
				required: true,
				rangelength: [5,20],
                                onkeyup: false,
                                loginNotInUse: true
                               /* remote: {
                                  url: "emailcheck.php" ,
                                  type: "post" ,
                                  complete: function(data){
                                  if( data.responseText != "true" ) {
                                     alert("Sorry mate, this email address was registered but never activated!");
                                  }
                                } */ 
                        }, password:{
				required: true,
				rangelength: [8,20]
			}, password_repeat:{
				equalTo: '#password'
			}, firstname:{
			}, lastname: {
			}, pesel:{
//				greaterThanZero: true
				required: true,
				validPESEL: true
			},birthdate:{
				date: true,
				dateFrom1900ToNow: true
			}
			, sex:{
			}, email:{
				required: true
			}, address:{
			        url: true
                        }, photo: {
				required: true,
				accept: "image/*"
			}
		}, // rules
		messages:{
			password: {
				rangelength: jQuery.validator.format("Password must have between {0} and {1} characters ")
			},
			password_repeat:{
				equalTo: 'Passwords do not match'
			}
		},//messages
		errorReplacement:function(error,element){
			if(element.is(':radio') || element.is(':checkbox')){
				error.appendTo(element.parent());
			} else{
				error.insertAfter(element);
			}
		}, //errorReplacement
/*                submitHandler: function (form) {
			//var formData = new FormData();
                        $.ajax({
				type: "POST",
				url: "http://edi.iem.pw.edu.pl/bach/register/user/",
				data: form.serialize() , //formData, //$('#signup input[name!=photo]').serialize();,
                                processData: false,
                                contentType: false, // 'multipart/form-data',
                                success: function (response) {
	                                alert(response); 
        /*				$(form).html("<div id='message'></div>");
					var message = document.createElement("p");
					var textNode = document.createTextNode("");
					var message = document.
					$('#message').html("<h2>Your request is on the way!</h2>")
						.append("<p>someone</p>")
						.hide()
						.fadeIn(1500, function () {
							$('#message').append("<img id='checkmark' src='images/ok.png' />");
						});
				
						}
			});
			return false; // required to block normal submit since you used ajax
		}
*/	});
}

function addCustomMethods(){
	jQuery.validator.addMethod("greaterThanZero", function(value, element) {
		return this.optional(element) || (parseFloat(value) > 0);
	}, "Value must be greater than zero");
	jQuery.validator.addMethod("validPESEL", function(value,element){
                if( this.optional(element)){
			return true;
		}
		if ( value.length != 11 || isNaN( value ) || parseInt(value) <= 0){
			return false;
		}
		var month = parseInt( value.substring(2,4) ) % 20;
		var day = parseInt( value.substring(4,6) );
		if ( month < 1 || month > 12 || day < 1 || day > 31){
			return false;
		}
		var multipliers = [3,1,9,7];
		var pesel = parseInt(value);
		var checksumCalculated = 0;
		var checksum = pesel % 10;
		pesel = Math.floor( pesel / 10 );
		var i = 0;

		while ( pesel > 0 ){
			checksumCalculated += (pesel % 10) * multipliers[i % multipliers.length];
			pesel = Math.floor( pesel / 10 );
			i++;
		}
		checksumCalculated %= 10;
		checksumCalculated = 10 - checksumCalculated;
		checksumCalculated %= 10;

		if ( checksumCalculated !== checksum ){
			return false;
		}
		return true;
	}, "PESEL is not valid");
	jQuery.validator.addMethod("dateFrom1900ToNow", function(value, element) {
		if ( this.optional(element) )
			return true;
		var year = parseInt(value.split("/")[2]);
	        var currentYear = new Date().getFullYear();
		return ( year >= 1900 && year < currentYear );
	}, "Please enter a valid year from 1900 - previous-year range");

        jQuery.validator.addMethod("loginNotInUse", function(value, element) {
                if ( this.optional(element) )
			return true;
		return getRemoteLoginValidity(value, element);
        	
        }, "This login is already taken");


}
function getRemoteLoginValidity(value,element){
                var username = value;
                console.log( username ); 
                var isFree = false;
                $.ajax({
                  type: "GET",
		  url: ("http://edi.iem.pw.edu.pl/bach/register/check/"+username) ,
                  data: "{}",
                  async: false,
                  contentType: "text/json; charset=utf-8",
                  dataType: "text",
                  success: function(data){
                    isFree = handleData(data, username);
                  }
                });
                console.log("SECOND");
                console.log(isFree);
                return isFree;
}

function handleData(responseMsg, username) {
  responseMsg = JSON.parse(responseMsg);
  console.log(responseMsg);
  console.log(username);
  var isFree = !responseMsg[username];
  console.log("FIRST");
  console.log(isFree);
  pause(1000);
  return isFree;
}


function pause(milliseconds) {
  var dt = new Date();
  while ((new Date()) - dt <= milliseconds) { /* Do nothing */ }
}

